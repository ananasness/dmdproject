import json

def setup_config(cfg):
    print("Enter parameters, empty for [default]")
    for key in cfg.keys():
        text = input("%s [%s]: " % (key, cfg[key]))
        if text != "" : cfg[key] = text

    return cfg

def merge_two_dicts(x, y):
    '''Given two dicts, merge them into a new dict as a shallow copy.'''
    z = x.copy()
    z.update(y)
    return z

database = {
    "DB_HOST": "localhost",
    "DB_PORT": "5432",
    "DB_USER": "postgres",
    "DB_NAME": "dmdproject",
    "DB_PASSWORD": ""
}

print("Hotel Management System and Monitoring Tools Setup")
print()

print("Config connection to PostresSQL")
setup_config(database)

with open('hms/config/database.json', 'w') as file:
    json.dump(database, file)

with open('monitor/config/database.json', 'w') as file:
    json.dump(database, file)

print("Config saved")