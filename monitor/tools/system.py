import datetime
import psutil
from time import time

def system_info():
    MBFACTOR = float(1 << 20)
    data = {
        "up_time": str(datetime.timedelta(seconds=int(time()) - psutil.boot_time())),

        "cpu_percent": psutil.cpu_percent(interval=1),

        "memory_physical": round(psutil.virtual_memory().total / MBFACTOR),
        "memory_available": round(psutil.virtual_memory().available / MBFACTOR),

        "disk_total": round(psutil.disk_usage('/').total / MBFACTOR),
        "disk_used": round(psutil.disk_usage('/').used / MBFACTOR),
        "disk_free": round(psutil.disk_usage('/').free / MBFACTOR),
        "disk_percent": psutil.disk_usage('/').percent}

    if data["cpu_percent"] > 95:  # High CPU load
        data["warring_cpu_load"] = True
    else:
        data["warring_cpu_load"] = False

    if data["memory_available"] < 100 : #100 Mb
        data["warring_free_memory"] = True
    else:
        data["warring_free_memory"] = False

    if data["disk_percent"]> 90: #10% of disk space is free:
        data["warring_disk_space"] = True
    else:
        data["warring_disk_space"] = False

    return data