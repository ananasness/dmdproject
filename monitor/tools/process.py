import psutil
import platform

MBFACTOR = float(1 << 20)

def process_info(pids):
    if type(pids) is not list: pids = [pids]  # convert to list
    info = []

    for pid in pids:

        p = psutil.Process(pid)
        with p.oneshot():
            data = {
                "pid": pid,

                "cpu_percent": p.cpu_percent(interval=1),

                "rss_memory": round(p.memory_info().rss / MBFACTOR),
                "vms_memory": round(p.memory_info().vms / MBFACTOR),

            }
            if platform.system() in ['Windows', 'Linux']:
                data["io_read_count"] = p.io_counters().read_count
                data["io_read_bytes"] = p.io_counters().read_bytes
                data["io_write_count"] = p.io_counters().write_count
                data["io_write_bytes"] = p.io_counters().write_bytes
            else:
                data["io_read_count"] = "Unsupported backend"
                data["io_read_bytes"] = "Unsupported backend"
                data["io_write_count"] = "Unsupported backend"
                data["io_write_bytes"] = "Unsupported backend"

        info.append(data)

    return info
