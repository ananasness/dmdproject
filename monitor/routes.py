def register_routes(app):
    from monitor.modules.front.views import mod as front
    app.register_blueprint(front)

    from monitor.modules.status.views import mod as status
    app.register_blueprint(status)

    from monitor.modules.pg_stat.views import mod as pg_stat
    app.register_blueprint(pg_stat)

    from monitor.modules.executor.views import mod as executor
    app.register_blueprint(executor)
