from flask import Blueprint, render_template
from flask_classy import FlaskView


from monitor.tools import system_info, process_info
from .models import PgProcess

mod = Blueprint('status', __name__)


class StatusView(FlaskView):
    def index(self):
        return render_template("status/status.html",
                               system=system_info(),
                               processes=process_info([x.pid for x in PgProcess.all()]))

StatusView.register(mod)
