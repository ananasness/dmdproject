from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired

class ExecuteForm(FlaskForm):
    query = StringField('query', validators=[DataRequired()])

class ExplainForm(ExecuteForm):
    pass
