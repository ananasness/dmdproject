from flask import Blueprint, render_template
from flask import redirect
from flask import url_for
from flask_classy import FlaskView, route
from .forms import ExecuteForm, ExplainForm
from orm.database import execute as db_execute
from monitor.model import Model

mod = Blueprint('executor', __name__)

class ExecutorView(FlaskView):
    @route('/')
    def index(self):
        return render_template('executor/index.html', execute_form=ExecuteForm(), explain_form=ExplainForm())

    @route('/execute', methods=['POST'])
    def execute(self):
        form = ExecuteForm()
        if form.validate_on_submit():
            out, err = db_execute(Model.connection, form.query.data)
            if err != None:
                # TODO proper handling of this error
                return redirect(url_for('executor.ExecutorView:index'))

            return render_template("executor/execute.html", input=out)

        return redirect(url_for('executor.ExecutorView:index'))

    @route('/explain', methods=['POST'])
    def explain(self):
        form = ExecuteForm()
        if form.validate_on_submit():
            query = 'EXPLAIN(ANALYZE ON, BUFFERS ON)' + form.query.data
            out, err = db_execute(Model.connection, query)
            if err != None:
                # TODO proper handling of this error
                return redirect(url_for('executor.ExecutorView:index'))

            return render_template("executor/explain.html", input=out)

        return redirect(url_for('executor.ExecutorView:index'))

ExecutorView.register(mod)
