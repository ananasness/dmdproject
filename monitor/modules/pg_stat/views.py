from flask import Blueprint, render_template
from flask_classy import FlaskView, route
from .models import get_model, models

mod = Blueprint('pgstat', __name__)

class PgstatView(FlaskView):
    @route('/')
    def index(self):
        return render_template("pg_stat/index.html", tables=models.keys())

    @route('/<table_name>')
    def show_table(self, table_name):
        model = get_model(table_name)
        objects = model.all()
        names = model.names
        return render_template("pg_stat/table.html", title=table_name, objects=objects, fields=names)


PgstatView.register(mod)