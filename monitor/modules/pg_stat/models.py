from monitor.model import Model
import sys

# Tools
class PgStatArchiver(Model):
    table_name = "pg_stat_archiver"


class PgStatBgWriter(Model):
    table_name = "pg_stat_bgwriter"


# Database wide
class PgStatDatabase(Model):
    table_name = "pg_stat_database"


class PgStatDatabaseConflict(Model):
    table_name = "pg_stat_database_conflicts"


# Tables
class PgStatSysTable(Model):
    table_name = "pg_stat_sys_tables"


class PgStatUserTable(Model):
    table_name = "pg_stat_user_tables"


class PgStatIOSysTable(Model):
    table_name = "pg_statio_sys_tables"


class PgStatIOUserTable(Model):
    table_name = "pg_statio_user_tables"


# Indexes
class PgStatUserIndex(Model):
    table_name = "pg_stat_user_indexes"


class PgStatSysIndex(Model):
    table_name = "pg_stat_sys_indexes"


class PgStatIOUserIndex(Model):
    table_name = "pg_statio_user_indexes"


class PgStatIOSysIndex(Model):
    table_name = "pg_statio_sys_indexes"


# Sequences
class PgStatIOUserSequence(Model):
    table_name = "pg_statio_user_sequences"


# User functions
class PgStatUserFunction(Model):
    table_name = "pg_stat_user_functions"


models = {
    "pg_stat_archiver": PgStatArchiver,
    "pg_stat_bgwriter": PgStatBgWriter,
    "pg_stat_database": PgStatDatabase,
    "pg_stat_database_conflicts": PgStatDatabaseConflict,
    "pg_statio_sys_indexes": PgStatIOSysIndex,
    "pg_statio_sys_tables": PgStatIOSysTable,
    "pg_statio_user_indexes": PgStatIOUserIndex,
    "pg_statio_user_sequences": PgStatIOUserSequence,
    "pg_statio_user_tables": PgStatIOUserTable,
    "pg_stat_sys_indexes": PgStatSysIndex,
    "pg_stat_sys_tables": PgStatSysTable,
    "pg_stat_user_functions": PgStatUserFunction,
    "pg_stat_user_indexes": PgStatUserIndex,
    "pg_stat_user_tables": PgStatUserTable
}


def get_model(str):
    return models.get(str)

