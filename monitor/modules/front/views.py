from flask import Blueprint, render_template
from flask_classy import FlaskView, route

mod = Blueprint('front', __name__)

class FrontView(FlaskView):
    route_base = '/'

    def index(self):
        return render_template("front/index.html")


FrontView.register(mod)