FROM alpine:edge
MAINTAINER Semyon Bessonov

RUN apk update
RUN apk upgrade
RUN apk add gcc musl-dev linux-headers python3 python3-dev libffi libffi-dev py-cffi py3-psycopg2

COPY libs.txt /application/
WORKDIR /application

RUN pip3 install -r libs.txt
RUN pip3 install gunicorn

ADD .    /application/

WORKDIR /application/
ENTRYPOINT python3 init_db.py && gunicorn -b 0.0.0.0:8000 -w 2 hms:app