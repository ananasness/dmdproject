import psycopg2


def connect(config):
    try:
        # params = \
        #     "host='%s' port='%s' dbname='%s' user='%s' password='%s'" % \
        #     (config["DB_HOST"], config["DB_PORT"], config["DB_NAME"], config["DB_USER"], config["DB_PASSWORD"])

        params = \
            "host='%s'" % (config["DB_HOST"]) \
            + "port='%s'" % (config["DB_PORT"]) \
            + "dbname='%s'" % (config["DB_NAME"]) \
            + "user='%s'" % (config["DB_USER"]) \
            + "password='%s'" % (config["DB_PASSWORD"]) \

        connection = psycopg2.connect(params)
    except:
        print("Could not connect to DB")
        raise

    return connection


def execute(conn, sql):
    cursor = conn.cursor()

    try:
        cursor.execute(str(sql))
        conn.commit()
    except Exception as e:
        conn.rollback()
        return None, str(e)

    return cursor.fetchall(), None


def init_schema(config):
    connection = connect(config)
    cursor = connection.cursor()

    try:
        schema = open(config["DB_SCHEMA"], "r").read()
        cursor.execute('''DROP SCHEMA public CASCADE;
                       CREATE SCHEMA public;
                       GRANT ALL ON SCHEMA public TO postgres;
                       GRANT ALL ON SCHEMA public TO public;
                       COMMENT ON SCHEMA public IS 'standard public schema'; ''')
        cursor.execute(schema)
        connection.commit()
    except:
        connection.rollback()
        raise


def init_seeds(config):
    connection = connect(config)
    cursor = connection.cursor()

    try:
        seeds = open(config["DB_SEEDS"], "r").read()
        cursor.execute(seeds)
        connection.commit()
    except:
        connection.rollback()
        raise

    cursor.close()
    connection.close()

