### This repository contains two apps: 
* Hotel Managment System
* Monitoring Tool

####To install you need:
* ```PostgresSQL 9.6+```: download: [win64](http://www.enterprisedb.com/postgresql-961-installers-win64?ls=Crossover&type=Crossover), [other variants](https://www.postgresql.org/download/) 
* ```Python 3.5+``` (with pip):  download: [win64]( https://www.python.org/ftp/python/3.5.2/python-3.5.2-amd64.exe), [other variants](https://www.python.org/downloads/)
* This repo (```git clone https://bitbucket.org/ananasness/dmdproject.git```) or download 

####What you need to prepare:
* Create new database and user with access to this database in postgres

####How to install:
* Open in console dir with project
* [Optional] create virtual environment 
    *  Install virtyalenv  ```pip install virtualenv```
    *  Create virtyalenv   ```virtualenv venv```
    *  Activate virtyalenv ```venv/Scripts/activate```
* Install all requirements ```pip install -r libs.txt```
* Start setup_db script and follow instructions ```setup_db.py```

####How to use:
* If virtualenv was created, check that it's activated
* Start ```hms.py``` for hotel management system, open [http://127.0.0.1:5001/](http://127.0.0.1:5001/)
* Or start  ```monitor.py``` for monitor tool, open [http://127.0.0.1:5002/](http://127.0.0.1:5002/)
* Hint! default  user:password for admin is ```owner:owner```  (relogin may help sometimes)
* For SQL Schema and Data go to hms/tools/db