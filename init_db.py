from orm.database import init_seeds, init_schema
import json

print("INIT DB SCHEMA")

with open('hms/config/schema.json', 'r') as file:
    schema = json.load(file)

with open('hms/config/database.json', 'r') as file:
    database = json.load(file)

config = {**schema, **database}
init_schema(config)
init_seeds(config)
