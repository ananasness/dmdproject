from flask_principal import Principal, Permission, RoleNeed, Identity, identity_changed, identity_loaded, UserNeed
from flask_login import LoginManager, current_user
from .app import app
from hms.modules.users.models import User

login_manager = LoginManager(app)
principals = Principal(app)


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)


@identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
    identity.user = current_user

    if hasattr(current_user, 'roles'):
        for role in current_user.roles:
            identity.provides.add(RoleNeed(role))


def build_permission(*roles):
    """
    management = build_permissions('owner', 'manager')
    `management` is Permission now
    wrap desired endpoint with management.require()

    @management.require()
    def index():
        pass

    thereby `index` is available only for owners and managers
    """
    return Permission(*[RoleNeed(role) for role in roles])
