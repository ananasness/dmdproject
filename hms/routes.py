from flask import request, redirect, flash

def register_routes(app):
    from hms.modules.front.views import mod as front
    app.register_blueprint(front)

    from hms.modules.auth.views import mod as auth
    app.register_blueprint(auth)

    from hms.modules.bookings.views import mod as bookings
    app.register_blueprint(bookings)

    from hms.modules.clients.views import mod as clients
    app.register_blueprint(clients)

    from hms.modules.hotels.views import mod as hotels
    app.register_blueprint(hotels)

    from hms.modules.managers.views import mod as managers
    app.register_blueprint(managers)

    from hms.modules.owners.views import mod as owners
    app.register_blueprint(owners)

    from hms.modules.receps.views import mod as receps
    app.register_blueprint(receps)

    from hms.modules.rooms.views import mod as rooms
    app.register_blueprint(rooms)

    from hms.modules.users.views import mod as users
    app.register_blueprint(users)

    @app.errorhandler(403)
    def unauthorized(e):
        flash("Oops! Access forbidden ")
        return redirect(request.referrer)
