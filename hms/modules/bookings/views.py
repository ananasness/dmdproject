from flask_classy import route
import datetime
from operator import attrgetter
from flask import Blueprint, render_template, request

from hms.modules.clients.models import Client
from hms.modules.rooms.models import Room
from hms.modules.users.models import User
from .models import Booking, BookingExtended
from ..model_view import ModelsView
from .forms import NewBookingForm
from hms.app import app
from .permissions import *
from flask_classy import route

mod = Blueprint('bookings', __name__, url_prefix='/hotels/<int:hotel_id>')


class BookingsView(ModelsView):

    model = Booking
    title = 'Bookings'
    template_dir = 'bookings'
    fields_for_show = ['room_number', 'date', 'client_name']
    edit_form = NewBookingForm
    new_form = NewBookingForm

    read_permission = read
    write_permission = write

    @route('/')
    def my_index(self, **kwargs):
        rooms = Room.getBy(hotel_id=kwargs['hotel_id'])
        bookings = BookingExtended.getBy(hotel_id=kwargs['hotel_id'])

        dates_range = []
        if bookings != []:
            dates_range = []
            start = min(bookings, key=attrgetter('date')).date
            end = max(bookings, key=attrgetter('date')).date
            step = datetime.timedelta(days=1)
            while start <= end:
                dates_range.append(start)
                start += step

        matrix = [ [[] for i in rooms] for j in dates_range]

        for di, d in enumerate(dates_range):
            for ri, r in enumerate(rooms):
                for b in bookings:
                    if b.date == d and b.room_number == r.number:
                        matrix[di][ri] = b

        return render_template('%s/table.html' % self.template_dir, title=self.title, rooms=rooms, matrix=matrix, dates_range=dates_range)

    def get_objects(self, **kwargs):
        page = kwargs.get('page', None)
        if page:
            objects = BookingExtended.fetchBy(page=kwargs['page'], hotel_id=kwargs['hotel_id'])
        else:
            objects = BookingExtended.getBy(hotel_id=kwargs['hotel_id'])
        return objects

    def get_obj(self, **kwargs):
        return BookingExtended.get(kwargs['id'])

    def set_values(self, obj, form, **kwargs):
        obj.client_id = int(form.client.data)
        obj.room_id = int(form.room.data)
        obj.date = form.date.data

    def fill_up_new_form(self, **kwargs):
        form = self.new_form()

        form.client.choices = [(client.id, User.get(client.user_id).name) for client in Client.all()]  # stupid access to client name
        form.room.choices = [(room.id, room.number) for room in Room.getBy(hotel_id=kwargs['hotel_id'])]

        return form

    def fill_up_edit_form(self, obj, **kwargs):
        form = self.new_form()

        form.client.choices = [(client.id, User.get(client.user_id).name) for client in
                               Client.all()]  # stupid access to client name
        form.room.choices = [(room.id, room.number) for room in Room.getBy(hotel_id=kwargs['hotel_id'])]

        form.client.data = obj.client_id
        form.room.data = obj.room_id
        form.date.data = obj.date

        return form

BookingsView.register(mod)

# print(type(Booking.get(0).date))
