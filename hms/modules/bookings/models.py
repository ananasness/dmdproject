import datetime

import psycopg2

from hms.model import Model
from hms.modules.rooms.models import Room
from hms.modules.users.models import User
from hms.modules.clients.models import Client
from orm.model import sql_str_value


class Booking(Model):

    def room(self):
        return Room.get(self.room_id)

    def client(self):
        return Client.get(self.client_id)

    @classmethod
    def get_by_date_period(cls, start_date, finish_date):
        try:
            cur = cls.connection.cursor()
            print("SELECT * FROM Bookings WHERE date > {} AND date < {}".format(sql_str_value(start_date), sql_str_value(finish_date)))
            cur.execute("SELECT * FROM Bookings WHERE date > {} AND date < {}".format(sql_str_value(start_date), sql_str_value(finish_date)))
            rows = cur.fetchall()
            return [cls.getObject(row) for row in rows]

        except psycopg2.IntegrityError as e:
            print(e)
            cls.connection.rollback()
        finally:
            cur.close()

# it's just stored here. Don't touch
view_query = """CREATE OR REPLACE VIEW bookingsview AS
                 SELECT b.id,
                    c.id AS client_id,
                    u.name AS client_name,
                    h.id as hotel_id,
                    r.id AS room_id,
                    r.number AS room_number,
                    b.date
                   FROM bookings b,
                    rooms r,
                    clients c,
                    users u,
                    hotels h
                  WHERE b.client_id = c.id AND b.room_id = r.id AND r.hotel_id = h.id AND c.user_id = u.id
"""

class BookingExtended(Model):

    table_name = 'BookingsView'
