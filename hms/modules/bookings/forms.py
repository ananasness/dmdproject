from wtforms import Form, StringField, validators, PasswordField, IntegerField, SelectField
from wtforms.fields.html5 import DateField

class NewBookingForm(Form):

    client = SelectField('Client', choices=[validators.DataRequired()])
    date = DateField('Date', validators=[validators.DataRequired()])
    room = SelectField('Room', choices=[validators.DataRequired()])


class EditUserForm(Form):

    name = StringField('Name', validators=[validators.DataRequired()])
    login = StringField('Login', validators=[validators.DataRequired()])
