from flask import Blueprint, render_template, Response
from flask_classy import FlaskView, route
from .permissions import *

mod = Blueprint('managers', __name__)

class ManagersView(FlaskView):
    def index(self):
        return render_template('managers/index.html')

ManagersView.register(mod)
