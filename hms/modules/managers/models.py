from hms.model import Model
from hms.modules.hotels.models import Hotel


class Manager(Model):

    def hotel(self):  # like foreign key
        return Hotel.get(hotel_id=self.hotel_id)