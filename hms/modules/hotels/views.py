from flask import Blueprint
from ..model_view import ModelsView
from hms.modules.hotels.forms import NewEditHotelForm
from hms.modules.hotels.models import Hotel
from .permissions import *

mod = Blueprint('hotels', __name__, url_prefix='')


class HotelsView(ModelsView):

    model = Hotel
    title = 'Hotel'
    template_dir = 'hotels'
    fields_for_show = ['name', 'address']
    edit_form = NewEditHotelForm
    new_form = NewEditHotelForm

    read_permission = read
    write_permission = write

HotelsView.register(mod)
