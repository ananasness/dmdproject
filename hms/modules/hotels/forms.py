from wtforms import Form, StringField, validators


class NewEditHotelForm(Form):

    name = StringField('name', validators=[validators.DataRequired()])
    address = StringField('address', validators=[validators.DataRequired()])
