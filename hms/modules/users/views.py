from flask import Blueprint
from flask_classy import route
from hms.modules.clients.models import Client
from hms.modules.hotels.models import Hotel
from hms.modules.managers.models import Manager
from hms.modules.owners.models import Owner
from hms.modules.receps.models import Recep
from hms.modules.users.permissions import read, write
from ..model_view import ModelsView
from .models import User
from .forms import NewUserForm, EditUserForm
from flask_login import current_user

mod = Blueprint('users', __name__, url_prefix='')


class UsersView(ModelsView):

    fields_for_show = ['name', 'login', 'roles', 'hotel']
    title = 'User'
    template_dir = 'users'
    model = User
    edit_form = EditUserForm
    new_form = NewUserForm
    read_permission = read
    write_permission = write

    def fill_up_edit_form(self, obj, **kwargs):

        form = self.edit_form()

        form.hotel.choices = [(hotel.id, hotel.name) for hotel in Hotel.all()]

        form.owner.data = bool(obj.owner)
        form.manager.data = bool(obj.manager)
        form.recep.data = bool(obj.recep)
        form.client.data = bool(obj.client)

        return form

    def get_obj(self, **kwargs):
        obj = User.get(kwargs['id'])
        if obj.manager:
            setattr(obj, 'hotel', Hotel.get(obj.manager.hotel_id).name)
        elif obj.recep:
            setattr(obj, 'hotel', Hotel.get(obj.recep.hotel_id).name)

        return obj

    def get_objects(self, **kwargs):
        page = kwargs.get('page', None)
        if page:
            objects = User.fetch_page(kwargs['page'])
        else:
            objects = User.all()
        for obj in objects:
            if obj.manager:
                setattr(obj, 'hotel', Hotel.get(obj.manager.hotel_id).name)
            elif obj.recep:
                setattr(obj, 'hotel', Hotel.get(obj.recep.hotel_id).name)

        return objects

    def set_values(self, obj, form, **kwargs):
        setattr(obj, 'name', form.name.data)
        setattr(obj, 'login', form.name.data)
        try:
            setattr(obj, 'password', form.password.data)
        except AttributeError:
            pass

        if obj.id == None:
            return obj

        if not ('manager' in current_user.roles or 'owner' in current_user.roles):
            return obj

        if bool(obj.client) != bool(form.client.data):
            if form.client.data:
                Client.new(user_id=obj.id)
            else:
                Client.deleteBy(user_id=obj.id)

        if obj.recep:
            if form.recep.data:
                Recep.update(Recep(id=obj.recep.id, hotel_id=int(form.hotel.data)))
            else:
                Recep.deleteBy(user_id=obj.id)
        else:
            if form.recep.data:
                Recep.new(user_id=obj.id, hotel_id=int(form.hotel.data))

        if 'owner' not in current_user.roles:
            return obj

        if obj.manager:
            if form.manager.data:
                Manager.update(Manager(id=obj.manager.id, hotel_id=int(form.hotel.data)))
            else:
                Manager.deleteBy(user_id=obj.id)
        else:
            if form.manager.data:
                Manager.new(user_id=obj.id, hotel_id=int(form.hotel.data))

        if bool(obj.owner) != bool(form.owner.data):
            if form.owner.data:
                Owner.new(user_id=obj.id)
            else:
                Owner.deleteBy(user_id=obj.id)

UsersView.register(mod)
