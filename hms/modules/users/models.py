from hms.model import Model
from hms.modules.clients.models import Client
from hms.modules.receps.models import Recep
from hms.modules.managers.models import Manager
from hms.modules.owners.models import Owner



class User(Model):
    def __repr__(self):
        return "<User %d, %s, %s>" % (self.id, self.name, self.login)

    @property
    def client(self):
        return first(Client.getBy(user_id=self.id))

    @property
    def recep(self):
        return first(Recep.getBy(user_id=self.id))

    @property
    def manager(self):
        return first(Manager.getBy(user_id=self.id))

    @property
    def owner(self):
        return first(Owner.getBy(user_id=self.id))

    @property
    def roles(self):
        res = []
        if self.client:
            res.append('client')
        if self.recep:
            res.append('recep')
        if self.manager:
            res.append('manager')
        if self.owner:
            res.append('owner')
        return res

    @property
    def is_active(self):
        return True

    @property
    def is_authenticated(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __eq__(self, other):
        if isinstance(other, UserMixin):
            return self.get_id() == other.get_id()
        return NotImplemented

    def __ne__(self, other):
        equal = self.__eq__(other)
        if equal is NotImplemented:
            return NotImplemented
        return not equal


def first(ls):
    if isinstance(ls, list) and ls:
        return ls[0]
    else:
        return None


# User.all()
# User.getBy(name = 'nikiton')
#
# user = User(id=1, login='coder2', name='mike', password='209403')
# user.save()
# user.drop()
#
# User.new(id=10, login='apple', name='apple', password='45678')




# user = User(id=20, name="Mansur", login='lalala')
# print(user)
# result = user.save()
#
# if result.success:
#     print("everything is ok")
# else:
#     print(result.error)
#
#
# user = User.get(0)
# print(user)
# owner = user.owner()
# print(owner)

# for i in range(8):
#     print(User.fetch_page(i))



