from wtforms import Form, StringField, validators, PasswordField, SelectMultipleField, BooleanField, SelectField


class NewUserForm(Form):

    name = StringField('name', validators=[validators.Length(min=1, max=30)])
    login = StringField('login', validators=[validators.Length(min=1, max=30)])
    password = PasswordField('password', validators=[validators.Length(min=1), validators.EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('confirm', validators=[validators.DataRequired()])


class EditUserForm(Form):

    name = StringField('name', validators=[validators.Length(min=1, max=30)])
    login = StringField('login', validators=[validators.Length(min=1, max=30)])
    owner = BooleanField('owner', validators=[])
    manager = BooleanField('manager', validators=[])
    recep = BooleanField('recep', validators=[])
    client = BooleanField('client', validators=[])
    hotel = SelectField('hotel', validators=[])