from flask import Blueprint, render_template, Response
from flask_classy import FlaskView, route
# from .forms import LoginForm
from .permissions import *

mod = Blueprint('clients', __name__)

class ClientsView(FlaskView):
    def index(self):
        return render_template('clients/index.html')

ClientsView.register(mod)
