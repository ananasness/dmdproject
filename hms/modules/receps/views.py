from flask import Blueprint, render_template, Response
from flask_classy import FlaskView, route
from .permissions import *

mod = Blueprint('receps', __name__)

class RecepsView(FlaskView):
    def index(self):
        return render_template('receps/index.html')

RecepsView.register(mod)
