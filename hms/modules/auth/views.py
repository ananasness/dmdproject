from flask import Blueprint, render_template, Response, redirect, url_for, request, session, current_app
from flask_classy import FlaskView, route
from flask_login import login_user, logout_user
from flask_principal import Identity, AnonymousIdentity, identity_changed

from .forms import LoginForm, SignUpForm

from hms.modules.users.models import User
from hms.modules.clients.models import Client

mod = Blueprint('auth', __name__)

class AuthView(FlaskView):
    def index(self):
        login_form = LoginForm()
        signup_form = SignUpForm()
        return render_template('auth/index.html', login_form=login_form, signup_form=signup_form)

    @route("/login", methods=['POST'])
    def login(self):
        form = LoginForm()
        if form.validate_on_submit():
            user = User.getBy(login=form.login.data)
            if not user:
                return redirect(url_for('auth.AuthView:index'))
            user = user[0]

            if form.password.data == user.password:
                # TODO: duplicate_1
                login_user(user)
                identity_changed.send(current_app._get_current_object(), identity=Identity(user.id))

                return redirect(request.args.get('next') or '/')
            return redirect(url_for('auth.AuthView:index'))
        return redirect(url_for('auth.AuthView:index'))

    @route("/signup", methods=['POST'])
    def signup(self):
        form = SignUpForm()
        if form.validate_on_submit():
            user = User(login=form.login.data, name=form.name.data, password=form.password.data)
            result = user.save()
            if result.error:
                return Response(result.error)

            user = User.getBy(login=form.login.data)[0]
            client = Client(user_id=user.id)

            result = client.save()
            if result.error:
                return Response(str(result.error))

            # TODO: duplicate_1
            login_user(user)
            identity_changed.send(current_app._get_current_object(), identity=Identity(user.id))
            return redirect('/')
        return redirect(url_for('auth.AuthView:index'))

    @route("/logout", methods=['POST'])
    def logout(self):
        logout_user()
        for key in ('identity.name', 'identity.auth_type'):
            session.pop(key, None)

        identity_changed.send(current_app._get_current_object(), identity=AnonymousIdentity())
        return redirect(url_for('auth.AuthView:index'))


AuthView.register(mod)