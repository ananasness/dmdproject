from hms.model import Model
from hms.modules.hotels.models import Hotel


class Room(Model):

    def hotel(self):
        return Hotel.get(self.hotel_id)
