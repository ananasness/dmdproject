from wtforms import Form, StringField, BooleanField, IntegerField, validators


class EditNewRoomForm(Form):

    capacity = IntegerField('capacity', validators=[validators.number_range(min=1)])
    number = IntegerField('number', validators=[validators.number_range(min=1)])
    available = BooleanField('available', validators=[])
    price = IntegerField('price', validators=[validators.number_range(min=0)])