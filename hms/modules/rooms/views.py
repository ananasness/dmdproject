from flask import Blueprint, render_template
from flask_classy import route
from hms.app import app
from ..model_view import ModelsView
from .models import Room
from .forms import EditNewRoomForm
from .permissions import *

mod = Blueprint('rooms', __name__, url_prefix='/hotels/<int:hotel_id>')


class RoomsView(ModelsView):

    model = Room
    title = 'Rooms'
    template_dir = 'rooms'
    fields_for_show = ['number', 'capacity', 'available', 'price']
    edit_form = EditNewRoomForm
    new_form = EditNewRoomForm

    read_permission = read
    write_permission = write

    def get_objects(self, **kwargs):
        page = kwargs.get('page', None)
        if page:
            objects = Room.fetchBy(page=kwargs['page'], hotel_id=kwargs['hotel_id'])
        else:
            objects = Room.getBy(hotel_id=kwargs['hotel_id'])
        return objects

    def set_values(self, obj, form, **kwargs):
        super().set_values(obj, form, **kwargs)
        obj.available = form.available.data
        obj.hotel_id = kwargs['hotel_id']

RoomsView.register(mod)
