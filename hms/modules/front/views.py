import time

import datetime

from flask import Blueprint, render_template, Response, redirect, url_for, request, session, abort, current_app, flash
from flask_classy import FlaskView, route
from flask_login import login_required, login_user, logout_user, current_user
from flask_principal import Identity, AnonymousIdentity, identity_changed
from hms.modules.bookings.models import Booking
from hms.modules.clients.models import Client
from hms.modules.rooms.models import Room

from .forms import FrontBookingForm
from .permissions import *

from hms.auth import login_manager
from hms.modules.users.models import User
from hms.modules.hotels.models import Hotel


mod = Blueprint('front', __name__)


class FrontView(FlaskView):

    route_base = '/'

    def index(self):
        booking_form = FrontBookingForm()
        # TODO: add today date
        # booking_form.check_in.data = time.time()
        booking_form.hotel.choices = [(hotel.id, "%s [%s] " % (hotel.name,hotel.address) ) for hotel in Hotel.all()]

        return render_template('front/index.html', booking_form=booking_form)

    def post(self):
        form = FrontBookingForm(request.form)
        # stupid fix by ananas instead of form.validate()
        if not form.check_in.data == None and not form.check_out.data == None :
            if current_user.is_authenticated:
                rooms = Room.getBy(capacity=form.beds.data, hotel_id=form.hotel.data)

                client = current_user.client

                if client == None:
                    client = Client(user_id=current_user.id)
                    client.save()

                if rooms == []:
                    flash("Sorry, we don't have suitable room for you. Try again later")
                    return redirect(url_for('front.FrontView:index'))

                start_date = form.check_in.data
                finish_date = form.check_out.data

                books = Booking.get_by_date_period(start_date=start_date,finish_date=finish_date)
                engaged_rooms_id = [book.room_id for book in books]

                for room in rooms:
                    if room.id not in engaged_rooms_id:
                        room_id = room.id
                        break
                else:
                    flash("Sorry, we don't have suitable room for you. Try again later")
                    return redirect(url_for('front.FrontView:index'))

                delta = 0
                start_date = form.check_in.data
                finish_date = form.check_out.data
                while start_date + datetime.timedelta(delta) <= finish_date:
                    Booking.new(client_id=client.id, room_id=room_id, date=start_date + datetime.timedelta(delta))
                    delta += 1

                print("*")
                flash("Successful booking")
                return redirect(url_for('front.FrontView:index'))

            flash("Please, login after booking")
            return redirect(url_for('auth.AuthView:login'))
        flash("Please fill up all fields")
        return redirect(url_for('front.FrontView:index'))







FrontView.register(mod)
