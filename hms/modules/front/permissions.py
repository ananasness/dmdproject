from flask_principal import Permission, RoleNeed
from hms.auth import build_permission

write = build_permission('owner', 'manager', 'recep', 'client')
read = build_permission('owner', 'manager', 'recep', 'client')
