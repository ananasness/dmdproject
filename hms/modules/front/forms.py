from wtforms.validators import DataRequired
from wtforms import Form, StringField, validators, PasswordField, IntegerField, SelectField
from wtforms.fields.html5 import DateField

class FrontBookingForm(Form):
    hotel = SelectField('Hotel', choices=[], validators=[DataRequired()])
    beds = IntegerField('Beds', validators=[DataRequired()])
    check_in = DateField('Check-in', validators=[DataRequired()])
    check_out = DateField('Check-out', validators=[DataRequired()])
