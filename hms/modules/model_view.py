from flask import flash, request

from flask import render_template, request, redirect, url_for
from flask_classy import FlaskView, route

size = 20

class ModelsView(FlaskView):

    fields_for_show = []
    template_dir = ""
    title = None
    model = None
    edit_form = None
    new_form = None

    read_permission = None
    write_permission = None

    @route('/')
    def my_index(self, **kwargs):
        with self.read_permission.require(http_exception=403):
            page = request.args.get('page', '')
            if page.isdigit():
                page = int(page)
            else:
                page = 1

            print(page)
            objects = (self.get_objects(page=page, **kwargs))
            return render_template('%s/index.html' % self.template_dir, title=self.title, fields=self.fields_for_show, objects=objects)

    @route('/<int:id>')
    def my_get(self, **kwargs):
        with self.read_permission.require(http_exception=403):
            return render_template('%s/show.html' % self.template_dir, title=self.title, fields=self.fields_for_show, obj=self.get_obj(**kwargs))

    @route('/new/', methods=['POST', 'GET'])
    def new(self, **kwargs):
        print(request.path)
        with self.write_permission.require(http_exception=403):
            if request.method == 'GET':
                form = self.fill_up_new_form(**kwargs)
                print(type(form))
                return render_template('%s/form.html' % self.template_dir, title=self.title, form=form, obj=None)

            elif request.method == 'POST':
                obj = self.model()

                form = self.new_form(request.form)
                if not form.validate():
                    flash("Invalid data in fields")
                    return redirect(request.path)
                else:
                    self.set_values(obj, form, **kwargs)
                    res = obj.save()
                    if not res.success:
                        flash(str(res.error))

            return redirect(url_for("{}.{}:my_index".format(self.template_dir, self.__class__.__name__), **kwargs))

    @route('/<id>/edit/', methods=['POST', 'GET'])
    def edit(self, **kwargs):
        with self.write_permission.require(http_exception=403):
            if request.method == 'GET':
                obj = self.get_obj(**kwargs)
                form = self.fill_up_edit_form(obj, **kwargs)
                return render_template('%s/form.html' % self.template_dir, title=self.title, form=form, obj=obj)

            elif request.method == 'POST':
                obj = self.model.get(kwargs['id'])

                form = self.edit_form(request.form)
                if not form.validate():
                    flash("Invalid data in the fields")
                    return redirect(request.path)
                else:
                    self.set_values(obj, form, **kwargs)

                    res = obj.save()
                    if not res.success:
                        flash(str(res.error))

            kwargs.pop('id')
            return redirect(url_for("{}.{}:my_index".format(self.template_dir, self.__class__.__name__), **kwargs))

    @route('/<int:id>/delete/', methods=['GET', 'POST'])
    def delete(self, **kwargs):
        with self.write_permission.require(http_exception=403):
            res = self.model.deleteBy(id=kwargs['id'])
            if not res.success:
                flash(str(res.error))

            kwargs.pop('id')
            return redirect(url_for("{}.{}:my_index".format(self.template_dir, self.__class__.__name__), **kwargs))

    def set_values(self, obj, form, **kwargs):
        for field in form:
            setattr(obj, field.name, getattr(form, field.name, None).data)

    def get_objects(self, **kwargs):
        page = kwargs.get('page', None)
        if page:
            objects = self.model.fetch_page(kwargs['page'])
        else:
            objects = self.model.all()

        return objects

    def get_obj(self, **kwargs):
        return self.model.get(kwargs['id'])

    def fill_up_edit_form(self, obj, **kwargs):
        form = self.edit_form()
        for field in form:
            field.data = getattr(obj, field.name, None)
        return form

    def fill_up_new_form(self, **kwargs):
        return self.new_form()
