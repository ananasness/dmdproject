from flask import Blueprint, render_template, Response
from flask_classy import FlaskView, route
from .permissions import *

mod = Blueprint('owners', __name__)

class OwnersView(FlaskView):
    def index(self):
        return render_template('owners/index.html')

OwnersView.register(mod)
