from flask_principal import Permission, RoleNeed
from hms.auth import build_permission

write = Permission(RoleNeed('owner'))
read = build_permission('owner', 'manager', 'recep')
