from flask import Flask
from .routes import register_routes

app = Flask(__name__)

app.config.from_json("config/default.json")
app.config.from_json("config/database.json")

"""
Config.json can be created to overwrite default options
For example check `default.json`
"""
app.config.from_json("config/config.json", silent=True)

"""
If need you can create env variable to point to some additional configs
"""
app.config.from_envvar("FLASK_CONFIG", silent=True)
register_routes(app)
