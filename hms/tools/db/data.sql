INSERT INTO users    (name, login, password) VALUES ('client', 'client', 'client');
INSERT INTO clients  (user_id) VALUES  (1);

--Owner
INSERT INTO users    (name, login, password) VALUES ('owner', 'owner', 'owner');
INSERT INTO owners   (user_id) VALUES (2);

--Stuff for hotel0
INSERT INTO users    (name, login, password) VALUES ('manager', 'manager', 'manager');
INSERT INTO users    (name, login, password) VALUES ('recept', 'recept', 'recept');

--Empty User
INSERT INTO users    (name, login, password) VALUES ('user', 'user', 'user');

--Hotel in Innopolis
INSERT INTO hotels   (name, address) VALUES ('Hotel 0', 'Innopolis');
INSERT INTO managers (user_id, hotel_id) VALUES (3, 1); --Manager
INSERT INTO receps   (user_id, hotel_id) VALUES (4, 1); --Receptionist

--Other Hotels (empty)
INSERT INTO hotels   (name, address) VALUES ('Hotel 1', 'Kazan');
INSERT INTO hotels   (name, address) VALUES ('Hotel 2', 'Ufa');
INSERT INTO hotels   (name, address) VALUES ('Hotel 3', 'Irkutsk');
INSERT INTO hotels   (name, address) VALUES ('Hotel 4', 'Yekaterinburg');
INSERT INTO hotels   (name, address) VALUES ('Hotel 5', 'Moscow');
INSERT INTO hotels   (name, address) VALUES ('Hotel 6', 'Saint Petersburg');
INSERT INTO hotels   (name, address) VALUES ('Hotel 7', 'Nizhny Novgorod');
INSERT INTO hotels   (name, address) VALUES ('Hotel 8', 'Samara');
INSERT INTO hotels   (name, address) VALUES ('Hotel 9', 'Omsk');

--rooms
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (1, 1, 1,   TRUE, 101);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (1, 2, 10,  TRUE, 102);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (1, 5, 100, TRUE, 103);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (1, 1, 1,   TRUE, 104);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (1, 2, 10,  TRUE, 105);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (1, 5, 100, TRUE, 106);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (1, 1, 1,   TRUE, 107);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (1, 2, 10,  TRUE, 108);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (1, 5, 100, TRUE, 109);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (1, 1, 1,   TRUE, 110);

INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (2, 1, 1,   TRUE, 101);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (2, 2, 10,  TRUE, 102);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (2, 5, 100, TRUE, 103);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (2, 1, 1,   TRUE, 104);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (2, 2, 10,  TRUE, 105);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (2, 5, 100, TRUE, 106);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (2, 1, 1,   TRUE, 107);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (2, 2, 10,  TRUE, 108);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (2, 5, 100, TRUE, 109);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (2, 1, 1,   TRUE, 110);

INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (3, 1, 1,   TRUE, 101);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (3, 2, 10,  TRUE, 102);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (3, 5, 100, TRUE, 103);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (3, 1, 1,   TRUE, 104);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (3, 2, 10,  TRUE, 105);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (3, 5, 100, TRUE, 106);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (3, 1, 1,   TRUE, 107);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (3, 2, 10,  TRUE, 108);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (3, 5, 100, TRUE, 109);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (3, 1, 1,   TRUE, 110);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (4, 1, 1,   TRUE, 101);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (4, 2, 10,  TRUE, 102);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (4, 5, 100, TRUE, 103);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (4, 1, 1,   TRUE, 104);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (4, 2, 10,  TRUE, 105);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (4, 5, 100, TRUE, 106);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (4, 1, 1,   TRUE, 107);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (4, 2, 10,  TRUE, 108);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (4, 5, 100, TRUE, 109);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (4, 1, 1,   TRUE, 110);

INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (5, 1, 1,   TRUE, 101);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (5, 2, 10,  TRUE, 102);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (5, 5, 100, TRUE, 103);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (5, 1, 1,   TRUE, 104);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (5, 2, 10,  TRUE, 105);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (5, 5, 100, TRUE, 106);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (5, 1, 1,   TRUE, 107);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (5, 2, 10,  TRUE, 108);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (5, 5, 100, TRUE, 109);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (5, 1, 1,   TRUE, 110);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (6, 1, 1,   TRUE, 101);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (6, 2, 10,  TRUE, 102);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (6, 5, 100, TRUE, 103);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (6, 1, 1,   TRUE, 104);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (6, 2, 10,  TRUE, 105);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (6, 5, 100, TRUE, 106);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (6, 1, 1,   TRUE, 107);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (6, 2, 10,  TRUE, 108);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (6, 5, 100, TRUE, 109);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (6, 1, 1,   TRUE, 110);

INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (7, 1, 1,   TRUE, 101);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (7, 2, 10,  TRUE, 102);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (7, 5, 100, TRUE, 103);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (7, 1, 1,   TRUE, 104);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (7, 2, 10,  TRUE, 105);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (7, 5, 100, TRUE, 106);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (7, 1, 1,   TRUE, 107);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (7, 2, 10,  TRUE, 108);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (7, 5, 100, TRUE, 109);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (7, 1, 1,   TRUE, 110);

INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (8, 1, 1,   TRUE, 101);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (8, 2, 10,  TRUE, 102);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (8, 5, 100, TRUE, 103);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (8, 1, 1,   TRUE, 104);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (8, 2, 10,  TRUE, 105);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (8, 5, 100, TRUE, 106);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (8, 1, 1,   TRUE, 107);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (8, 2, 10,  TRUE, 108);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (8, 5, 100, TRUE, 109);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (8, 1, 1,   TRUE, 110);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (9, 1, 1,   TRUE, 101);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (9, 2, 10,  TRUE, 102);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (9, 5, 100, TRUE, 103);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (9, 1, 1,   TRUE, 104);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (9, 2, 10,  TRUE, 105);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (9, 5, 100, TRUE, 106);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (9, 1, 1,   TRUE, 107);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (9, 2, 10,  TRUE, 108);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (9, 5, 100, TRUE, 109);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (9, 1, 1,   TRUE, 110);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (10, 1, 1,   TRUE, 101);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (10, 2, 10,  TRUE, 102);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (10, 5, 100, TRUE, 103);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (10, 1, 1,   TRUE, 104);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (10, 2, 10,  TRUE, 105);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (10, 5, 100, TRUE, 106);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (10, 1, 1,   TRUE, 107);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (10, 2, 10,  TRUE, 108);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (10, 5, 100, TRUE, 109);
INSERT INTO rooms (hotel_id, capacity, price, available, number) VALUES (10, 1, 1,   TRUE, 110);

--Book client in hotel 0
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 1,  	date '2016-12-24');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 1,  	date '2016-12-25');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 1,  	date '2016-12-26');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 2,  	date '2016-12-25');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 2,  	date '2016-12-26');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 2,  	date '2016-12-27');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 3,  	date '2016-12-25');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 3,  	date '2016-12-26');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 3,  	date '2016-12-27');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 1,  	date '2016-12-28');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 1,  	date '2016-12-29');
INSERT INTO bookings (client_id, room_id, date) VALUES (1, 1,  	date '2016-12-30');

--GENERATED BOOKS--