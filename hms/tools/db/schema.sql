CREATE TABLE public.users
(
    id SERIAL PRIMARY KEY NOT NULL,
    name TEXT,
    login TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL
);

CREATE TABLE public.hotels
(
    id SERIAL PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    address TEXT NOT NULL
);

CREATE TABLE public.owners
(
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INT NOT NULL,
    CONSTRAINT owners_users_user_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE public.clients
(
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INT NOT NULL,
    CONSTRAINT clients_users_user_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE public.managers
(
    id SERIAL PRIMARY KEY NOT NULL,
    hotel_id INT NOT NULL,
    user_id INT NOT NULL,
    CONSTRAINT managers_users_user_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT managers_hotels_hotel_id_fk FOREIGN KEY (hotel_id) REFERENCES hotels (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE public.receps
(
    id SERIAL PRIMARY KEY NOT NULL,
    hotel_id INT NOT NULL,
    user_id INT NOT NULL,
    CONSTRAINT receps_users_user_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT receps_hotels_hotel_id_fk FOREIGN KEY (hotel_id) REFERENCES hotels (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE public.rooms
(
    id SERIAL PRIMARY KEY NOT NULL,
    hotel_id INT NOT NULL,
    capacity INT NOT NULL,
    price INT NOT NULL,
    available BOOL DEFAULT TRUE  NOT NULL,
    number INT NOT NULL,
    UNIQUE (hotel_id, number),
    CHECK (price > 0),
    CONSTRAINT rooms_hotels_hotel_id_fk FOREIGN KEY (hotel_id) REFERENCES hotels (id) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE public.bookings
(
    id SERIAL PRIMARY KEY NOT NULL,
    client_id INT NOT NULL,
    room_id INT NOT NULL, --Can be null, not the best variant, but what if hotel deleted ?
    date DATE NOT NULL,
    UNIQUE (room_id, date),
    CONSTRAINT bookings_rooms_room_id_fk FOREIGN KEY (room_id) REFERENCES rooms (id) ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT bookings_clients_client_id_fk FOREIGN KEY (client_id) REFERENCES clients (id) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE OR REPLACE VIEW bookingsview AS
 SELECT b.id,
    c.id AS client_id,
    u.name AS client_name,
    h.id as hotel_id,
    r.id AS room_id,
    r.number AS room_number,
    b.date
   FROM bookings b,
    rooms r,
    clients c,
    users u,
    hotels h
  WHERE b.client_id = c.id AND b.room_id = r.id AND r.hotel_id = h.id AND c.user_id = u.id;
