import datetime
from shutil import copyfile

# This script will produce data.sql file with more that 1ml inserts

copyfile("data_head.sql", "data.sql")

with open('data.sql', 'a') as file:
    for i in range(1, 1000001):
        file.write("INSERT INTO users (name, login, password) VALUES ('%s', '%s', '%s'); \n" % ("user" + str(i), "user" + str(i), "user" + str(i)))
